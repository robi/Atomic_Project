-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 10:29 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_birthday`
--

CREATE TABLE IF NOT EXISTS `tbl_birthday` (
  `birthday_id` int(3) NOT NULL,
  `birthday_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_birthday`
--

INSERT INTO `tbl_birthday` (`birthday_id`, `birthday_date`) VALUES
(1, '1990-11-05'),
(2, '0000-00-00'),
(3, '0000-00-00'),
(4, '0000-00-00'),
(5, '0000-00-00'),
(6, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_book`
--

CREATE TABLE IF NOT EXISTS `tbl_book` (
  `book_id` int(3) NOT NULL,
  `book_title` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_book`
--

INSERT INTO `tbl_book` (`book_id`, `book_title`) VALUES
(1, 'javascript'),
(2, 'Ajax1'),
(3, 'VP Dot Net'),
(4, 'Codeignitor'),
(5, 'Laravel'),
(6, ' C for beginners '),
(7, ' HTML '),
(8, ' Ajax tutorials ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE IF NOT EXISTS `tbl_city` (
  `city_id` int(3) NOT NULL,
  `city_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`city_id`, `city_name`) VALUES
(1, ' Dhaka'),
(2, 'Chittagong'),
(3, ' Rajshahi'),
(4, 'Sylhet'),
(5, '  Dhaka , ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_email`
--

CREATE TABLE IF NOT EXISTS `tbl_email` (
  `email_id` int(3) NOT NULL,
  `email_address` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_email`
--

INSERT INTO `tbl_email` (`email_id`, `email_address`) VALUES
(1, 'admin@pondit.com'),
(2, ' msdhonicse@yahoo.com '),
(3, ' biswassonjoy@yahoo.com ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gender`
--

CREATE TABLE IF NOT EXISTS `tbl_gender` (
  `gender_id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gender`
--

INSERT INTO `tbl_gender` (`gender_id`, `name`, `gender`) VALUES
(2, 'sonjoy biswas', 2),
(3, 'MIta', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hobby`
--

CREATE TABLE IF NOT EXISTS `tbl_hobby` (
  `hobby_id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `hobby` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_hobby`
--

INSERT INTO `tbl_hobby` (`hobby_id`, `name`, `hobby`) VALUES
(1, 'sonjoy', 'Playing Cricket,Watching Movie'),
(2, 'sonjoy biswas', ''),
(3, 'Chayan', 'Watching Movie,Programming'),
(4, 'Zitu', 'Playing Cricket,Watching Movie,Playing with Code'),
(6, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image`
--

CREATE TABLE IF NOT EXISTS `tbl_image` (
  `image_id` int(3) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_image`
--

INSERT INTO `tbl_image` (`image_id`, `name`, `image`) VALUES
(3, '', 'bitm_logo.gif'),
(4, ' sonjoy biswas', 'flannel-shirt.jpg'),
(6, ' ', '1432951168.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mobile`
--

CREATE TABLE IF NOT EXISTS `tbl_mobile` (
  `mobile_id` int(3) NOT NULL,
  `mobile_model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_mobile`
--

INSERT INTO `tbl_mobile` (`mobile_id`, `mobile_model`) VALUES
(1, ' Nokia'),
(2, ' Walton'),
(3, ' Samsung'),
(4, ' Micromax'),
(5, ' OPPO new');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_summary`
--

CREATE TABLE IF NOT EXISTS `tbl_summary` (
  `summary_id` int(3) NOT NULL,
  `summary` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_summary`
--

INSERT INTO `tbl_summary` (`summary_id`, `summary`) VALUES
(1, 'à¦¬à¦—à§à§œà¦¾à¦° à¦®à¦¹à¦¾à¦¸à§à¦¥à¦¾à¦¨à¦—à§œà§‡ à¦¸à§à¦¬à¦°à§à¦£à§‡à¦° à¦¦à§‹à¦•à¦¾à¦¨à§‡à¦° à¦¶à¦¾à¦°à§à¦Ÿà¦¾à¦° à¦à¦¬à¦‚ à¦¤à¦¾à¦²à¦¾ à¦•à§‡à¦Ÿà§‡ à¦¨à¦—à¦¦ à¦Ÿà¦¾à¦•à¦¾ à¦“ à§­à§¨ à¦­à¦°à¦¿ à¦¸à§à¦¬à¦°à§à¦£ à¦šà§à¦°à¦¿ à¦•à¦°à§‡ à¦¨à¦¿à§Ÿà§‡ à¦—à§‡à¦›à§‡ à¦šà§‹à¦°à§‡à¦°à¦¾à¥¤ à¦†à¦œ à¦¬à§à¦§à¦¬à¦¾à¦° à¦¸à¦•à¦¾à¦²à§‡ à¦¦à§‹à¦•à¦¾à¦¨ à¦®à¦¾à¦²à¦¿à¦• à¦•à¦°à§à¦®à¦šà¦¾à¦°à§€à¦°à¦¾ à¦à¦¸à§‡ à¦¦à§‹à¦•à¦¾à¦¨à§‡à¦° à¦¶à¦¾à¦°à§à¦Ÿà¦¾à¦°, à¦•à§à¦²à¦¾à¦ªà¦¸à¦¿à¦¬à¦² à¦—à§‡à¦Ÿ à¦­à¦¾à¦™à§à¦—à¦¾ à¦¦à§‡à¦–à¦¤à§‡ à¦ªà¦¾à§Ÿà¥¤ à¦ªà¦°à§‡ à¦¦à§‹à¦•à¦¨ à¦˜à¦°à§‡à¦° à¦­à¦¿à¦¤à¦°à§‡ à¦—à¦¿à§Ÿà§‡ à¦¦à§à¦Ÿà¦¿ à¦¸à¦¿à¦¨à§à¦¦à§à¦• à¦¥à§‡à¦•à§‡ à¦¸à§à¦¬à¦°à§à¦£à¦²à¦‚à¦•à¦¾à¦° à¦“ à¦•à¦¿à¦›à§ à¦¨à¦—à¦¦ à¦Ÿà¦¾à¦•à¦¾ à¦šà§à¦°à¦¿à¦° à¦˜à¦Ÿà¦¨à¦¾ à¦œà¦¾à¦¨à¦¤à§‡ à¦ªà¦¾à¦°à§‡à¥¤\r\n\r\nà¦œà¦¾à¦¨à¦¾ à¦¯à¦¾à§Ÿ, à¦¬à¦—à§à§œà¦¾à¦° à¦¸à¦¦à¦°à§‡à¦° à¦“ à¦¶à¦¿à¦¬à¦—à¦žà§à¦œ à¦¥à¦¾à¦¨à¦¾à¦° à¦¸à§€à¦®à¦¾à¦¨à§à¦¤ à¦à¦²à¦¾à¦•à¦¾ à¦®à¦¹à¦¾à¦¸à§à¦¥à¦¾à¦¨à¦—à§œà§‡à¦° à¦ªà¦¾à¦¶à§‡ à¦¬à¦—à§à§œà¦¾ à¦°à¦‚à¦ªà§à¦° à¦®à¦¹à¦¾à¦¸à§œà¦•à§‡à¦° à¦†à¦ªà¦¨ à¦œà§à§Ÿà§‡à¦²à¦¾à¦°à§à¦¸ à¦¨à¦¾à¦® à¦¦à¦¿à§Ÿà§‡ à¦¦à§€à¦°à§à¦˜à¦¦à¦¿à¦¨ à¦¥à§‡à¦•à§‡ à¦¬à§à¦¯à¦¬à¦¸à¦¾ à¦•à¦°à§‡'),
(2, ' Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  '),
(3, ' Company Organization '),
(4, '  	Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero  ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_birthday`
--
ALTER TABLE `tbl_birthday`
  ADD PRIMARY KEY (`birthday_id`);

--
-- Indexes for table `tbl_book`
--
ALTER TABLE `tbl_book`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `tbl_city`
--
ALTER TABLE `tbl_city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `tbl_email`
--
ALTER TABLE `tbl_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `tbl_gender`
--
ALTER TABLE `tbl_gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `tbl_hobby`
--
ALTER TABLE `tbl_hobby`
  ADD PRIMARY KEY (`hobby_id`);

--
-- Indexes for table `tbl_image`
--
ALTER TABLE `tbl_image`
  ADD PRIMARY KEY (`image_id`);

--
-- Indexes for table `tbl_mobile`
--
ALTER TABLE `tbl_mobile`
  ADD PRIMARY KEY (`mobile_id`);

--
-- Indexes for table `tbl_summary`
--
ALTER TABLE `tbl_summary`
  ADD PRIMARY KEY (`summary_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_birthday`
--
ALTER TABLE `tbl_birthday`
  MODIFY `birthday_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_book`
--
ALTER TABLE `tbl_book`
  MODIFY `book_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_city`
--
ALTER TABLE `tbl_city`
  MODIFY `city_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_email`
--
ALTER TABLE `tbl_email`
  MODIFY `email_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_gender`
--
ALTER TABLE `tbl_gender`
  MODIFY `gender_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_hobby`
--
ALTER TABLE `tbl_hobby`
  MODIFY `hobby_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_image`
--
ALTER TABLE `tbl_image`
  MODIFY `image_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_mobile`
--
ALTER TABLE `tbl_mobile`
  MODIFY `mobile_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_summary`
--
ALTER TABLE `tbl_summary`
  MODIFY `summary_id` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
