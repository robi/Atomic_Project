<?php

namespace App\Bitm\SEIP106392\place;

include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class City {

    public $id;
    public $city;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

//    public function __construct($city_name = false) {
//        $this->city=$city_name ;
//    }

    public function index() {
        $data = array();
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_city`";
        $result = mysql_query($query);
        while($row = mysql_fetch_object($result)){
            $data[] = $row;
        }
        return $data;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store($city = false) {
        Connection::db_connect();
        $city = $city['city_name'];
        $query = "INSERT INTO `tbl_city`(`city_name`) VALUES (' " . $city . " ')";
        
        $result = mysql_query($query);
//        var_dump($result);
//        exit();
        if ($result) {
            $message = "Your City name is inserted !!";
        } else {
            $message = "Your City name is not inserted !!";
        }
        Utility::redirect2();
    }

    public function edit() {
        echo " I am editing form";
    }

    public function update($data = null) {
        Connection::db_connect();
//         echo '<pre>';
//        print_r($data);
//       exit();
        $id = $data['city_id'];
        $name = $data['city_name'];
        
        $query = "UPDATE `tbl_city` SET `city_id`='".$id."',`city_name`='".$name."' WHERE `city_id`=".$id;
//        Utility::debug($query);
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect2();
    }

    public function delete($city_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_city`WHERE `tbl_city`.`city_id`=".$city_id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Your data is successfully Deleted!!");
        }else{
            Utility::message("Unable to delete!!");
        }
        Utility::redirect2();
    }
    
    public function view($city_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_city` WHERE `tbl_city`.`city_id`=".$city_id;
         
        $result = mysql_query($query);
       
        $row = mysql_fetch_object($result);
        return $row;
    }

}
