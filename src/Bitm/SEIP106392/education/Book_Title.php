<?php

namespace App\Bitm\SEIP106392\education;

//include_once '../../../vendor/autoload.php';
//namespace App\Bitm\SEIP106392\db\Connection;
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Book_Title {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($title=false) {
        $this->title=$title;
    }

    public function index() {
        $data=array();
        Connection::db_connect();
        $query="SELECT * FROM `tbl_book`";
        $result=  mysql_query($query);
        while($row= mysql_fetch_object($result)){
            $data[]=$row;
        }
        return $data;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store() {

        Connection::db_connect();
        $query="INSERT INTO `tbl_book`( `book_title`) VALUES (' ".$this->title." ')";
        
        $result=  mysql_query($query);
        //var_dump($result);
        if($result){
            Utility::message("Book title is inserted successfully!!");
        }else{
            Utility::message("Book title is not inserted!!");
        }
        Utility::redirect1();
        
    }

    public function edit($data= false) {
        $id=$data['book_id'];
        $title=$data['book_title'];
        Connection::db_connect();
        
        $query = "UPDATE `tbl_book` SET `book_id`='".$id."',`book_title`='".$title."' WHERE `book_id`= ".$id;
       
        $result = mysql_query($query);
//         echo '<pre>';
//        print_r($result);
//        exit();
        if($result){
            Utility::message("Your data is successfully Updated!!");
        }else{
            Utility::message("Unable to update!!");
        }
        Utility::redirect1(); 
    }

    public function view( $book_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_book` WHERE `tbl_book`.`book_id` =".$book_id;
        
        $result = mysql_query($query);
        $row = mysql_fetch_object($result);
        return $row;
    }

    public function delete($book_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_book` WHERE `tbl_book`.`book_id` =".$book_id;
        
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Deleted !!");
        }else{
            Utility::message("Unable to Delete");
        }
        Utility::redirect1();
    }

}
