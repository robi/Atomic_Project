<?php

namespace App\Bitm\SEIP106392\person;
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Gender {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($gender = false) {
        ;
    }

    public function index() {
        $data=array();
        Connection::db_connect();
        $query="SELECT * FROM `tbl_gender`";
        $result=  mysql_query($query);
        while($row= mysql_fetch_object($result)){
            $data[]=$row;
        }
        return $data;
    }
    public function view($gender_id = null){
        Connection::db_connect();
       $query = "SELECT * FROM `tbl_gender` WHERE `tbl_gender`.`gender_id`=".$gender_id;
        $result = mysql_query($query);
        $row=  mysql_fetch_object($result);
        return $row;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store($gender = null) {
        Connection::db_connect();
//        Utility::debug($hobby);
        $name= $gender['name'];
        $gender= $gender['gender'];
        
        $query = "INSERT INTO `tbl_gender`(`name`, `gender`) VALUES ('".$name."','".$gender."')";
//        Utility::debug($query);
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your Gender is successfully Inserted");
        }else{
            Utility::message("Unable to Insert");
        }
        Utility::redirect7();
    }

    public function edit() {
        echo " I am editing form";
    }

  
     public function update($gender = null) {
         Connection::db_connect();
//       Utility::debug($gender);
         $id = $gender['gender_id'];
         $name = $gender['name'];
        $gender = $gender['gender'];
        $query = "UPDATE `tbl_gender` SET `gender_id`='".$id."',`name`= '".$name."',`gender`='".$gender."' WHERE `gender_id`=".$id;
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your Data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect7();
    }

    public function delete($gender_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_gender` WHERE `tbl_gender`.`gender_id`=".$gender_id;
        $result = mysql_query($query);
//        Utility::debug($result);
        
        if($result){
            Utility::message("Your Gender is successfully Deleted");
        }else{
            Utility::message("Unable to Delete");
        }
        Utility::redirect7();
    }

}
