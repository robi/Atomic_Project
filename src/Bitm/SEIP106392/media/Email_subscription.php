<?php

namespace App\Bitm\SEIP106392\media;

include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Email_subscription {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($email = false) {
        //var_dump($email);
        $this->email = $email[ 'email_address' ];
    }

    public function index() {
        $data = array();
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_email`";
        $result = mysql_query($query);
        while ($row = mysql_fetch_object($result)) {
            $data[] = $row;
        }
        return $data;
    }

    public function create() {
        echo "  I am create form";
    }

    public function store() {
        Connection::db_connect();
        $query = "INSERT INTO `tbl_email`( `email_address`) VALUES (' ".$this->email." ')";
        $result = mysql_query($query);
//        var_dump($result);
        if ($result) {
            Utility::message("Your Email Address is inserted !!");
        } else {
            Utility::message("Your Email Address  is not inserted !!");
        }
       Utility::redirect4();
    }

    public function edit() {
        echo " I am editing form";
    }

    public function update($data = null) {
        Connection::db_connect();
//         echo '<pre>';
//        print_r($data);
//       exit();
        $id = $data['email_id'];
        $email = $data['email_address'];
        
        $query = "UPDATE `tbl_email` SET `email_id`='".$id."',`email_address`='".$email."' WHERE `email_id`=".$id;
//        Utility::debug($query);
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect4();
    }

    public function delete($email_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_email`WHERE `tbl_email`.`email_id`=".$email_id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Your data is successfully Deleted!!");
        }else{
            Utility::message("Unable to delete!!");
        }
        Utility::redirect4();
    }
     public function view($email_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_email` WHERE `tbl_email`.`email_id`=".$email_id;
         
        $result = mysql_query($query);
       
        $row = mysql_fetch_object($result);
        return $row;
    }

}
