<?php

namespace App\Bitm\SEIP106392\celebration;

include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\db\Connection;
use \App\Bitm\SEIP106392\utility\Utility;

class Birthday {

    public $id;
    public $title;
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_at;

    public function __construct($date = false) {
       $this->date = $date;
    }

    public function index() {
        $data = array();
        Connection::db_connect();
         $query = "SELECT * FROM `tbl_birthday`";
         $result =  mysql_query($query);
         while($row =  mysql_fetch_object($result)){
             $data[] = $row;
         }
         return $data;
         
    }

    public function create() {
        echo "  I am create form";
    }

    public function store() {
        Connection::db_connect();
        $query = "INSERT INTO `tbl_birthday`( `birthday_date`) VALUES (' ". $this->date."')";
        $result =  mysql_query($query);
        if($result){
            Utility::message("Your Birthday Date is inserted !!");
        }else{
            Utility::message("Your Birthday Date is not inserted !!");
        }
        Utility::redirect();
    }

    public function update($data = null) {
        Connection::db_connect();
//         echo '<pre>';
//        print_r($data);
//       exit();
        $id = $data['birthday_id'];
        $date = $data['birthday_date'];
        
        $query = "UPDATE `tbl_birthday` SET `birthday_id`='".$id."',`birthday_date`='".$date."' WHERE `birthday_id`=".$id;
//        Utility::debug($query);
        $result = mysql_query($query);
        
        if($result){
            Utility::message("Your data is successfully Updated");
        }else{
            Utility::message("Unable to Update");
        }
        Utility::redirect();
    }

    public function delete($birthday_id = null) {
        Connection::db_connect();
        $query = "DELETE FROM `tbl_birthday`WHERE `tbl_birthday`.`birthday_id`=".$birthday_id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Your data is successfully Deleted!!");
        }else{
            Utility::message("Unable to delete!!");
        }
        Utility::redirect();
    }
    
    public function view($birthday_id = null) {
        Connection::db_connect();
        $query = "SELECT * FROM `tbl_birthday` WHERE `tbl_birthday`.`birthday_id`=".$birthday_id;
         
        $result = mysql_query($query);
       
        $row = mysql_fetch_object($result);
        return $row;
    }

}
