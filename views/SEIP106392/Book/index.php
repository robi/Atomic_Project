<?php ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book Title</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                    include_once '../../../vendor/autoload.php';

                    use \App\Bitm\SEIP106392\utility\Utility;
                    use \App\Bitm\SEIP106392\education\Book_Title;

$book = new Book_Title();
                    $data = $book->index();
                    $u = Utility::message();
//                    
                    ?>
                    <h1 class="text-center">Book List</h1>
                    <?php
                    if (isset($u)) {
                        ?>
                        <div class="alert alert-success" role="alert"><h4><?php echo $u; ?></h4></div>
                        <?php
                    }
                    ?>




                    <a href="create.php" class="btn btn-primary add">Add</a>
                    <a href="#" class="btn btn-primary add">Download As Pdf</a>


                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Book Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $sl = 1;
                            foreach ($data as $book) {
                                ?>

                                <tr>
                                    <td><?php echo $sl; ?></td>
                                    <td><a href="view.php?id=<?php echo $book->book_id;?>"><?php echo $book->book_title; ?></a></td>
                                    <td>
                                        <a href="view.php?id=<?php echo $book->book_id;?>"><button type="button" class="btn btn-success">View</button></a>
                                        <a href="edit.php?id=<?php echo $book->book_id;?>"><button type="button" class="btn btn-primary">Edit</button></a>
                                        <a href="delete.php?id=<?php echo $book->book_id; ?>" class="btn btn-danger " id="delete">Delete</a>
                                        <!--                                            <form action="delete.php" method="post">
                                                                                        <input type="hidden" name="book_id" value="<?php echo $book->book_id; ?>">
                                                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                        
                                                                                    </form>-->
                                        <a href="delete.php"><button type="button" class="btn btn-warning">Trash/Recover</button></a>
                                        <a href="delete.php"><button type="button" class="btn btn-default">Email to Friend</button></a>
                                    </td>
                                </tr>  
                                <?php
                                $sl++;
                            }
                            ?>

                        </tbody>
                    </table>
                    <nav>
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>

        </div>


        <script type="text/javascript" src="stylesheet" href="../../../assets/js/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#delete").click(function () {
                    confirm("Are you sure to Delete it?");
                });
            });
        </script>
    </body>
</html>
