<?php
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\place\City;

$city = new City();
$data = $city->view($_REQUEST['id']);
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>City</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="text-center">City</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your City:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" class="form-control" name="city_id" value="<?php echo $data->city_id;?>">
                                            <input type="text" class="form-control" name="city_name" value="<?php echo $data->city_name;?>"id="exampleInputEmail1" placeholder="City">
                                        </div>

                                        <div class="col-md-6 ">
                                             <button type="submit" class="btn btn-primary"><strong><i class="fa fa-edit"></i> Edit</strong></button>                                                       
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>

