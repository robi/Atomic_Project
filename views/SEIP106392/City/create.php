
<html>
    <head>
        <meta charset="UTF-8">
        <title>City</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                        include_once '../../../vendor/autoload.php';
                        use App\Bitm\SEIP106392\place\City;
                        $city= new City();                        
                    ?>
                    <h1 class="text-center">City</h1>
                    <h4 class="text-center"><?php echo $city->create();;?></h4>
                    <form action="store.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your City:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="text" class="form-control" name="city_name" id="exampleInputEmail1" placeholder="City">
                                        </div>

                                        <div class="col-md-6 ">
                                             <button type="submit" class="btn btn-primary"><strong><i class="fa fa-floppy-o"></i> Save</strong></button>                                                      
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
