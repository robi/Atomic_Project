<html>
    <head>
        <meta charset="UTF-8">
        <title>Gender</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                    include_once '../../../vendor/autoload.php';

                    use App\Bitm\SEIP106392\people\Hobby;

$hob = new Hobby();
                    ?>
                    <h1 class="text-center">Gender</h1>
                    <form action="store.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Name:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="date" class="form-control" name="name" id="exampleInputEmail1" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <div class="radio">
                                                    <label for="exampleInputEmail1">Select Your Gender:</label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="gender" id="inlineRadio1" value="1"> Male
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="gender" id="inlineRadio2" value="2"> Female
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <button type="submit"  name="save" class="btn btn-primary"><strong><i class="fa fa-floppy-o"></i> Save</strong></button>                            
                                            </div>
                                        </div>
                                    </div>
                                    <a href="index.php" class="btn btn-primary">Go to list</a>  
                                </div>
                            </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
