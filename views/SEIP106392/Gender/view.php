<?php
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\person\Gender;

$gender = new Gender();
$data = $gender->view($_REQUEST['id']);
?>
<label>Gender Id</label>
<h4><?php echo $data->gender_id; ?></h4>
<label>Name</label>
<h4><?php echo $data->name; ?></h4>
<label>Gender</label>
<h4><?php
if ($data->gender == 1) {
    echo "Male";
} else {
    echo "Female";
}
?></h4>