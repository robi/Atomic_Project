
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                        include_once '../../../vendor/autoload.php';
                        
                        use App\Bitm\SEIP106392\media\Email_subscription;
                        $obj=new Email_subscription();                       
                    ?>
                    <h1 class="text-center">Email Subscription</h1>
                    <h4 class="text-center"><?php echo $obj->create();?></h4>
                    <form action="store.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Email:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="text" class="form-control" name="email_address" id="exampleInputEmail1" placeholder="Email Address">
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary">Save</button>                            
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
