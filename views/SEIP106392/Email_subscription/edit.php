<?php

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP106392\media\Email_subscription;

$email = new Email_subscription();
$data= $email->view($_REQUEST['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Email</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
    </head>
    <body>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    
                    <h1 class="text-center">Email</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Edit Your Email:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" name="email_id" value="<?php echo $data->email_id;?>">
                                            <input type="text" class="form-control" name="email_address" id="exampleInputEmail1" value="<?php echo $data->email_address;?>" placeholder="Email Address">
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary">Edit</button>                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8  col-md-offset-3 m_top_30">
                                        <a href="index.php" class="btn btn-primary"><strong><i class="fa fa-list-alt"></i> List</strong></a>
                                        <a href="delete.php?id=<?php echo $value->email_id; ?>" class="btn btn-danger"><strong><i class="fa fa-trash"></i> Delete</strong></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
