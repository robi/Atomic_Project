<!doctype Html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Profile Picture</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                    include_once '../../../vendor/autoload.php';

                    use App\Bitm\SEIP106392\upload\Image;

$image = new Image();
                    ?>
                    <h1 class="text-center">Profile Picture</h1>
                    
                    <form action="store.php" method="post"  enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <label>Enter your name:</label>
                                            <input type="text"  name="name" class="form-control" placeholder="Name"> 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Please Select an Image:</label>
                                            <input type="file"  name="image_name" > 
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary" name="upload"><strong><i class="fa fa-upload"></i> Upload</strong></button>                            
                                        </div>
                                    </div>
                                </div>
                                <a href="index.php" class="btn btn-primary">Go to list</a>  
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
