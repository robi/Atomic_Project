<?php

include_once '../../../vendor/autoload.php';

use App\Bitm\SEIP106392\device\Mobile;

$mob = new Mobile();
$data= $mob->view($_REQUEST['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mobile Model</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/font-awesome.min.css"/>
    </head>
    <body>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    
                    <h1 class="text-center">Mobile Model</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Edit Your Mobile Model:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" name="mobile_id" value="<?php echo $data->mobile_id;?>">
                                            <input type="text" class="form-control" name="mobile_model" id="exampleInputEmail1" value="<?php echo $data->mobile_model;?>" placeholder="Mobile Model">
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary">Save</button>                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8  col-md-offset-3 m_top_30">
                                        <a href="index.php" class="btn btn-primary"><strong><i class="fa fa-list-alt"></i> List</strong></a>
                                        <a href="delete.php?id=<?php echo $value->mobile_id; ?>" class="btn btn-danger"><strong><i class="fa fa-trash"></i> Delete</strong></a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
