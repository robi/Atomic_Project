<?php

include_once '../../../vendor/autoload.php';

 use App\Bitm\SEIP106392\organization\Summary_of_Organization;

$summary = new Summary_of_Organization();
$data= $summary->view($_REQUEST['id']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Company Description</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">
                    <h1 class="text-center">Company Description</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Summary:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" class="form-control" value="<?php echo $data->summary_id;?>" name="summary_id" id="exampleInputEmail1" >
                                            <textarea  name="summary"   class="form-control" placeholder="Company Description" rows="8"><?php echo $data->summary;?></textarea>
                                        </div>

                                        <div class="col-md-6 ">
                                            <button type="submit" class="btn btn-primary">Edit</button>                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8  col-md-offset-3 m_top_30">
                                        <a href="index.php" class="btn btn-primary">List</a>
                                        <a href="delete.php" class="btn btn-danger">Delete</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </body>
</html>
