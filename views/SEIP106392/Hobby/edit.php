<?php
include_once '../../../vendor/autoload.php';

use \App\Bitm\SEIP106392\people\Hobby;
use \App\Bitm\SEIP106392\utility\Utility;

//echo '<pre>';
//print_r($_POST);
//exit();
$bday = new Hobby();

$hobbies = $bday->view($_GET['id']);
$son = explode(",", $hobbies->hobby);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hobby</title>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/bootstrap.min.css"/>
        <link type="text/css" rel="stylesheet" href="../../../assets/css/style.css"/>
    </head>
    <body>
        <a href="../../../index.php"><button type="button" class="btn btn-success">Home</button></a>
        <div class="container bg">
            <div class="row upper">
                <div class="col-md-10 col-md-offset-1">

                    <h1 class="text-center">Hobby</h1>
                    <form action="update.php" method="post">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-3 m_top_30">
                                <label for="exampleInputEmail1">Enter Your Name:</label>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6 ">
                                            <input type="hidden" class="form-control" name="hobby_id" value="<?php echo $hobbies->hobby_id ?>">
                                            <input type="text" class="form-control" name="name" value="<?php echo $hobbies->name ?>" id="exampleInputEmail1" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-12 ">
                                                <label for="exampleInputEmail1">Select Your Hobby:</label>
                                                <div class="checkbox">
                                                </div>

                                                <?php
//                                                Utility::debug($son);
                                                if (in_array("Playing Cricket", $son)) {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="Playing Cricket" name="hobby[]" id="checkbox-inline" checked="checked">
                                                        Playing Cricket
                                                    </label>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" value="Playing Cricket" name="hobby[]" id="checkbox-inline">
                                                        Playing Cricket
                                                    </label>
                                                    <?php
                                                }if (in_array("Watching Movie", $son)) {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox"  name="hobby[]" value="Watching Movie" id="checkbox-inline" checked="checked">Watching Movie
                                                    </label>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox"  name="hobby[]" value="Watching Movie" id="checkbox-inline">Watching Movie
                                                    </label>
                                                    <?php
                                                }
                                                if (in_array("Playing with Code", $son)) {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" name="hobby[]" value="Playing with Code" id="checkbox-inline" checked="checked">Playing with Code
                                                    </label>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" name="hobby[]" value="Playing with Code" id="checkbox-inline" >Playing with Code
                                                    </label>
    <?php
}if (in_array("Programming", $son)) {
    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox"  name="hobby[]" value="Programming" id="checkbox-inline" checked="checked">Programming
                                                    </label>
    <?php
} else {
    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox"  name="hobby[]" value="Programming" id="checkbox-inline" >Programming
                                                    </label>
    <?php
}if (in_array("Facebook", $son)) {
    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" name="hobby[]" value="Facebook" id="checkbox-inline" checked="checked">Facebook
                                                    </label>
    <?php
} else {
    ?>
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" name="hobby[]" value="Facebook" id="checkbox-inline" >Facebook
                                                    </label>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <button type="submit"  name="save" class="btn btn-primary"><strong><i class="fa fa-edit"></i> Edit</strong></button>                            
                                            </div>
                                        </div>
                                    </div>
                                    <a href="index.php" class="btn btn-primary">Go to list</a>  
                                </div>
                            </div>


                        </div>
                    </form>
                </div>

            </div>
    </body>
</html>


